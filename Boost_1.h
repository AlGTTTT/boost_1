#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>

///////////////////////////////////
 #define COMMENT_OUT_ALL_PRINTS

#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define FEA_DISACTIVATED (-2)
#define NUMBER_OF_ACTIVE_FEAS_IS_INSUFFICIENT (-3)

#define NO_SUITABLE_MODEL (-4)

#define NO_VALID_ELEMENTS (-5)

#define FEA_FOR_TRIAL 1
#define FEA_NOT_FOR_TRIAL 0

#define INITIAL_TEST_TRAN_DATASETS_INCLUDED 

//#define TESTING_TRAIN_VECS_WITHOUT_UPDATING

//used in 'PassAgg_Shifted_Optimized'
	#define USE_NORMALIZATION_TO_MEAN_0_AND_STDEV_1

	//#define INITIAL_ORTHONORMALIOZATION
//#define NORMALIOZATION_IN_OrthonormalizationOfVectorsByGrammSchimdt
#define USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY

//#define TESTING_TRAIN_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL
//#define TESTING_TEST_VECS_WITHOUT_UPDATING_BY_READING_A_MODEL

	//#define INCLUDING_EFFICIENT_MODELS

//#define PRINT_DETAILS
//////////////////////////////////////////////////////////////////////
//#define fWeight_Train 0.5 //0.7
#define fWeight_Train 0.6
//#define fWeight_Train 0.75

//#define fWeight_Test 0.5 //0.3
#define fWeight_Test 0.4 
//#define fWeight_Test 0.25 //0.3

#endif //#ifdef USE_WIGHTED_SUM_OF_TRAINING_AND_TESTING_FOR_MODEL_EFFICIENCY
//#define EXIT_AFTER_TRAINING

#define CALCULATING_Z_SCORES_OF_ALL_FEAS

//#define fLarge_ForReading 10.0
#define fLarge_ForReading 2.0

#define fZ_ScoreLimit 1.5

///////////////////////////////////////////////

#define nLarge 1000000
#define fLarge 1.0E+12 //9

#define NO_VecInit
//#define PRINT_DEBUGGING_INFO
#define eps 1.0E-9

#define fLimitForOrthogonalization 1.0E-1

#define fFeaMin (-5000.0)
#define fFeaMax 5000.0

//#define nLengthOneLineMax 1000 //400 //200 //120000 //50000
#define nLengthOneLineMax 4000 //400 //200 //120000 //50000

#define nInputLineLengthMax (nLengthOneLineMax) //200 //100
#define nSubstringLenMax 20
///////////////////////////////////////////////////

//#define nNumVecTrainTot 4178 // for svmguide1_train_2000_2178.txt
//#define nNumVecTrainTot 3089 // for svmguide1_train.txt
//#define nNumVecTrainTot 5267 //3089
//#define nNumVecTrainTot 540 //
//#define nNumVecTrainTot 500 //250 + 250
//#define nNumVecTrainTot 920 //(460+460) 
//#define nNumVecTrainTot 1676 //(838+838) 
//#define nNumVecTrainTot 1594 //(838+838) 
//#define nNumVecTrainTot 800
//#define nNumVecTrainTot 1300

//#define nNumVecTestTot 4000
//#define nNumVecTestTot 84 //60 + 22
//#define nNumVecTestTot 273 //63 + 210 //288 //78 + 210 //209 //94 + 115
//#define nNumVecTestTot 280 //
//#define nNumVecTestTot 160 //
//#define nNumVecTestTot 170 //
#define nNumVecTestTot 866 //
////////////////////////////////////////

//nNumOfPosTrainVecs + nNumOfNegTrainVecs = nNumVecTrainTot
#define nNumOfPosTrainVecs 650
#define nNumOfNegTrainVecs 650

#define nNumVecTrainTot (nNumOfPosTrainVecs + nNumOfNegTrainVecs)
////////////////////////////////////////////////////////////////////
//change 'nLengthOneLineMax' as well
//#define nDim 20 //10 //4 //8000
//#define nDim_DifEvo 200 
//#define nDim_DifEvo 50 //100 //50
#define nDim_DifEvo 100 //50

//#define nDim_DifEvo 120 //50
//#define nDim_DifEvo 15 //100 //50
#define nDim_DifEvo_WithConst (nDim_DifEvo + 1) //8000

//#define nNumOfActiveFeasTot_Min 6 //2 // < nDim_FeasInit
#define nNumOfActiveFeasTot_Min 3 //2 // < nDim_FeasInit

//for reading init data
#define nProdTrain_DifEvoTot (nDim_DifEvo*nNumVecTrainTot)
#define nProdTest_DifEvoTot (nDim_DifEvo*nNumVecTestTot)

#define nProd_WithConstTrainTot (nDim_DifEvo_WithConst*nNumVecTrainTot)
#define nProd_WithConstTestTot (nDim_DifEvo_WithConst*nNumVecTestTot)

////////////////////////////////////////////
//#define nDim_FeasInit 30 // <= nDim_DifEvo
//#define nDim_FeasInit 15 // <= nDim_DifEvo
#define nDim_FeasInit 5 // <= nDim_DifEvo

////////
#define fFeaRangeMin 0.0
#define fFeaRangeMax 1.0

#define fFeaDeviat 0.1  
//#define fFeaDeviat 0.02  

#define fFeaRangeMin_WithDeviat (fFeaRangeMin - fFeaDeviat)
#define fFeaRangeMax_WithDeviat (fFeaRangeMax + fFeaDeviat)

//#define fCoefForTrial 0.85
#define fCoefForTrial 0.3
#define fProbOfFeaForTrialMax 0.2
//////////////
//No generations
//#define nNumOfGenerTot 50

#define nNumOfVecsInOnePopulTot 80000 
//#define nNumOfVecsInOnePopulTot 40000 // > nNumOfModelsMax = 20
//#define nNumOfVecsInOnePopulTot 8000 // > nNumOfModelsMax = 20
//#define nNumOfVecsInOnePopulTot 800 // > nNumOfModelsMax = 20

#define nDimOfAllFeas_inOnePopul (nDim_FeasInit*nNumOfVecsInOnePopulTot)

//#define nDimOfAllFeas_inAllGeners (nDim_FeasInit*nNumOfVecsInOnePopulTot*nNumOfGenerTot)

#define fWidthOfIntervalForOneFea ((fFeaRangeMax - fFeaRangeMin)/nDim_DifEvo)

////////////////////////////
#define nPositOfFeaBeyondRange_Min ((int)(fFeaRangeMin_WithDeviat/fWidthOfIntervalForOneFea))

#define nPositOfFeaBeyondRange_Max ((int)(fFeaRangeMax_WithDeviat/fWidthOfIntervalForOneFea))

#define nLowestNumberMax 8 // < nDim_DifEvo

#define nNumOfFeaIfThereAreNoFeas 2 // < nDim_DifEvo

/////////////////////////////
//#define nDimOfAllVecs_inAllGeners (nNumOfVecsInOnePopulTot*nNumOfGenerTot)

#define nDim_D (nDim_FeasInit) //15
#define nDim_D_WithConst (nDim_FeasInit + 1) //16
///////////////////////////////////////////////////////////////////////////

#define nNumOfFitnessOfOneFeaVecTot_ForPrinting (-1)
//dimension of the nonlinear/transformed space

#define nDim_H 4
//#define nDim_H 8
//#define nDim_H 16
//#define nDim_H 32

//#define nNumOfHyperplanes_Min 2 //4 //3
//#define nNumOfHyperplanes_Max 64 //128 //256 //512
//#define nNumOfHyperplanes_Max 32 //128 //256 //512
//#define nNumOfHyperplanes_Max 8 //16 
//#define nNumOfHyperplanes_Max 4 
//#define nNumOfHyperplanes_Max 2 

//#define nNumOfHyperplanes_Factor 2

#define nNumOfHyperplanes 2

#define nK (nNumOfHyperplanes)
////////////////////

//#define nNumOfItersOfTrainingTot 8 //10
//define nNumOfItersOfTrainingTot 4 //-- OK
//#define nNumOfItersOfTrainingTot 2 //3 //10#define nNumOfItersOfTrainingTot 1 //10 //5 
#define nNumOfItersOfTrainingTot 1

//////////////////////////////////////////////////////////////////
#define fW_Init_Min (-5.0) //(-2.0) 
#define fW_Init_Max (5.0) //2.0
/////////////////////////////////
//#define nDim_U_Init (nDim_D_WithConst*nDim_H*nK)

/////////////////////////////////////////////////////////
#define fU_Init_Min (-4.0) //--the last
#define fU_Init_Max 4.0 

#define fBiasForClassifByLossFunction 0.0

#define fFeaConst_UnRefined_Init 2.0 // fFeaConst_ForRefined_Glob_Min <= fFeaConst_UnRefined_Init <= fFeaConst_ForRefined_Glob_Max
///////////////////////////////
#define nSrandInit_Unrefin_Min 1

#define nSrandInit__Unrefin_Max 500

#define nSrandInit_Step 1
////////////////////////////////////////

#define nNumOfItersForAnInitiallySuitableModelMax 5

#define nNumOfHyperplanes_Min 2  //3
#define nNumOfHyperplanes_Max 2 //4 

#define nNumOfHyperplanes_Factor 2

#define fW_Init_HalfRange_Min 0.5
#define fW_Init_HalfRange_Max 5.0

#define fW_Init_HalfRange_Step 0.5
/////////////////////////////////

//#define nDim_U (nDim_D_WithConst*nDim_H*nK)

/////////////////////////////////////////////////////////

#define fU_Init_HalfRange_Min 0.5
#define fU_Init_HalfRange_Max 5.0

#define fU_Init_HalfRange_Step 0.5
//////////////////////////////////////////////
// change 'nNumVecTrainTot' as well

///////////////////////////////////////

#define fAlpha 0.9 //const float fAlphaf, // < 1.0
#define fEpsilon 0.05 //?? -- linearly depends on variance

#define fCr 0.125
#define fC 0.125

#define fScalarProdOfNormalizedHyperplanesMax 0.1

///////////////////////////
//#define fR_Const 0.61803399
#define fC_Const ( (-1.0 + sqrt(5.0) )/2.0 ) //(1.0-R)
#define fR_Const (1.0 - fC_Const)

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define SHFT2(a,b,c) (a)=(b);(b)=(c);
#define SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

#define fEfficOfArr1stRequired 0.9

const float fPrecisionOf_Golden_Search = 0.01; // 0.005
const float eps_thesame = 1.0E-5; //-2; // precision of the position
/////////////////////////////////////////
//reading the model
#define nDim_Selec_Read (nDim_FeasInit) //15 //14
#define nDim_SelecFeas_WithConst_Read (nDim_Selec_Read + 1)

#define nProdSelec_WithConstTrainTot (nDim_SelecFeas_WithConst_Read*nNumVecTrainTot)
#define nProdSelec_WithConstTestTot (nDim_SelecFeas_WithConst_Read*nNumVecTestTot)

#define nDim_H_Read (nDim_H) // == 4 //8 //32

#define nK_Read (nK) // == nNumOfHyperplanes == 2

#define nDim_U_Read (nDim_D_WithConst*nDim_H*nK) //== 48 //256 //960
/////////////////////////////////////////////////////////////////////////

//#define fPercentageOfCorrect_Train_And_TestTot_Min 101.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 102.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 110.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 120.0  
#define fPercentageOfCorrect_Train_And_TestTot_Min 140.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 145.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 150.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Min 153.0  

//#define fPercentageOfCorrect_Train_And_TestTot_Max 110.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Max 120.0  
//#define fPercentageOfCorrect_Train_And_TestTot_Max 130.0  
#define fPercentageOfCorrect_Train_And_TestTot_Max 170.0  

// > fPercentageOfCorrect_Train_And_TestTot_Min and < fPercentageOfCorrect_Train_And_TestTot_Max
#define fPercentageOfCorrect_TrainMin_ToSelectAModel 142.0  
//#define fPercentageOfCorrect_TrainMin_ToSelectAModel 153.0  
//#define fPercentageOfCorrect_TrainMin_ToSelectAModel 155.0  

//combined with 'nDiffBetween_ClassifResultsTotMin' and 
// 'nNumOfModelsMax' to select the next model
//#define fPercentageOfCorrect_TrainMin_ToSelectAModel_ByMinDiffBetween_ClassifRes 150.0  //<= fPercentageOfCorrect_TrainMin_ToSelectAModel
#define fPercentageOfCorrect_TrainMin_ToSelectAModel_ByMinDiffBetween_ClassifRes 140.0  //<= fPercentageOfCorrect_TrainMin_ToSelectAModel

///////////////////////////////////////////////////////
//#define nNumOfModelsMax 400 // < nNumOfVecsInOnePopulTot = 8000
//#define nNumOfModelsMax 200 // < nNumOfVecsInOnePopulTot = 8000

#define nNumOfModelsMax 100 // < nNumOfVecsInOnePopulTot = 8000
//#define nNumOfModelsMax 50 // < nNumOfVecsInOnePopulTot = 800
//#define nNumOfModelsMax 20 // < nNumOfVecsInOnePopulTot = 800
//#define nNumOfModelsMax 8 // < nNumOfVecsInOnePopulTot = 800
//#define nNumOfModelsMax 5 // < nNumOfVecsInOnePopulTot = 800

//#define fCoefForWeightOfAModel 0.05
//#define fCoefForWeightOfAModel 0.1
#define fCoefForWeightOfAModel 0.5

//should be combined with 'fPercentageOfCorrect_TrainMin_ToSelectAModel_ByMinDiffBetween_ClassifRes' and 
// 'nNumOfModelsMax' to select the next model
#define nDiffBetween_ClassifResultsTotMin 30
//#define nDiffBetween_ClassifResultsTotMin 20
//#define nDiffBetween_ClassifResultsTotMin 10
//#define nDiffBetween_ClassifResultsTotMin 5
//#define nDiffBetween_ClassifResultsTotMin 2
//#define nDiffBetween_ClassifResultsTotMin 1 //all results as of Jan 8, 2021

//#define nNumOfItersForWeights 100
//#define nNumOfItersForWeights 90
//#define nNumOfItersForWeights 60
//#define nNumOfItersForWeights 30
#define nNumOfItersForWeights 10

//0 - no normalization, 1 - from 0 to 1, 2 - to the sum of 1
#define nTypeOfNormalizationForWeightsOfVectors 0 

//#define nSufficientNumOf_CorrectlyClassified_PosVecs 645 //<= nNumOfPosTrainVecs == 650
//#define nSufficientNumOf_CorrectlyClassified_PosVecs 640 //<= nNumOfPosTrainVecs == 650
//#define nSufficientNumOf_CorrectlyClassified_PosVecs 630 //<= nNumOfPosTrainVecs == 650
//#define nSufficientNumOf_CorrectlyClassified_PosVecs 620 //<= nNumOfPosTrainVecs == 650
#define nSufficientNumOf_CorrectlyClassified_PosVecs 610 //<= nNumOfPosTrainVecs == 650

//#define nSufficientNumOf_CorrectlyClassified_NegVecs 645 //<= nNumOfNegTrainVecs
//#define nSufficientNumOf_CorrectlyClassified_NegVecs 640 //<= nNumOfNegTrainVecs
//#define nSufficientNumOf_CorrectlyClassified_NegVecs 630 //<= nNumOfNegTrainVecs
//#define nSufficientNumOf_CorrectlyClassified_NegVecs 620 //<= nNumOfNegTrainVecs
#define nSufficientNumOf_CorrectlyClassified_NegVecs 610 //<= nNumOfNegTrainVecs

//#define fWeightedErrorMin 100.0
//#define fWeightedErrorMin 10.0
//#define fWeightedErrorMin 0.9
//#define fWeightedErrorMin 0.8
//#define fWeightedErrorMin 0.7
#define fWeightedErrorMin 0.5

//#define fClassifEfficiencyAverForPrintMin 0.85
#define fClassifEfficiencyAverForPrintMin 0.83

typedef struct
{
	int nNumOfPos_Y_Totf;
	int	nNumOfNeg_Y_Totf;

	int	nNumOfCorrect_Y_Totf;
	int	nNumOfVecs_Totf;

	int	nNumOfPosCorrect_Y_Totf;
	int	nNumOfNegCorrect_Y_Totf;

	float fPercentageOfCorrectTotf;
	float fPercentageOfCorrect_Posf;
	float fPercentageOfCorrect_Negf;

	int nClassifResultsPosArrf[nNumOfPosTrainVecs]; //[nNumOfPosTrainVecs] 1 -- correct, 0-- not
	int nClassifResultsNegArrf[nNumOfNegTrainVecs]; //[nNumOfNegTrainVecs], 1 -- correct, 0-- not

	float fLossesPosArrf[nNumOfPosTrainVecs]; //[nNumOfPosTrainVecs]
	float fLossesNegArrf[nNumOfNegTrainVecs]; //[nNumOfNegTrainVecs]

} PAS_AGG_RESUTS_WITH_LOSSES;
//////////////////////////////////////

typedef struct
{
	float fPercentageOfCorrectTot_Train;
		float fPercentageOfCorrectTot_Test;

		float fFeaConstInit;

		int nDim_SelecFeas;
		int nDim_SelecFeasWithConst; // = dimension of the original space

		int nPosOfSelec_FeasArr[nDim_Selec_Read]; //[nNumOfSelecFeasTotCurf]

		int nDim_H_Model; //int nDim_H_Read; //dimension of the nonlinear/transformed space

		int nK_Model; //int nK_Read; //nNumOfHyperplanes
		
		int nDim_U_Model; //int nDim_U_Read; //(nDim_D_WithConst*nDim_H*nK)
//////////////////////////////////////
	//	float fFeaSelecMin_TrainArr[nDim_Selec_Read]; //[nDim_SelecFeas_Readf]
	//	float fFeaSelecMax_TrainArr[nDim_Selec_Read]; //[nDim_SelecFeas_Readf]

	//	float fMean_Selec_Feas_TrainArr[nDim_Selec_Read]; //[nDim_SelecFeas_Readf]
	//	float fStDev_Selec_Feas_TrainArr[nDim_Selec_Read]; //[nDim_SelecFeas_Readf]

//////////////////////////////////////
		float fW_Train_Read_Arr[nDim_H_Read]; //[nDim_H_Read]
		float fU_Train_Read_Arr[nDim_U_Read]; //[nDim_U_Read]
////////////////////////////////

		int nClassifResultsPosArr[nNumOfPosTrainVecs]; //1 -- correct, 0-- not
		int nClassifResultsNegArr[nNumOfNegTrainVecs]; //0 -- correct, 1-- not

		float fLossesPosArr[nNumOfPosTrainVecs];
		float fLossesNegArr[nNumOfNegTrainVecs];

} ONE_MODEL_PASS_AGG;